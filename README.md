
# <a name="top"> </a>CONTENTS OF THIS FILE

 * [Introduction](#introduction)
 * [Requirements](#requirements)
 * [Installation](#installation)
 * [Configuration](#configuration)
 * [Features](#features)
 * [Troubleshooting](#troubleshooting)
 * [Roadmap](#roadmap)
 * [FAQ](#faq)
 * [Maintainers](#maintainers)

***
***
# <a name="introduction"></a>INTRODUCTION
Provides Mason integration to create a perfect gapless grid of elements.
This is not Masonry, or Isotope or Gridalicious. Mason fills in those ugly gaps,
and creates a perfectly filled space.

The module provides a Views style plugin to return results as a Mason grid.

***
## <a name="first"> </a>FIRST THINGS FIRST!
Read more at:
* [Github](https://git.drupalcode.org/project/blazy/-/blob/3.0.x/docs/README.md#first-things-first)
* [Blazy UI](/admin/help/blazy_ui#first)

***
***
# <a name="requirements"> </a>REQUIREMENTS
1. [Blazy](https://www.drupal.org/project/blazy)
2. Views module in core.
3. Mason library:
   + Download Mason archive from https://github.com/DrewDahlman/Mason
   * Extract it as is, rename "Mason-master" to "mason", so the assets
     are available at:

     `/libraries/mason/dist/mason.min.js`

***
***
# <a name="installation"> </a>INSTALLATION
Install the module as usual, more info can be found on:

[Installing Drupal 8 Modules](https://drupal.org/node/1897420)


***
***
# <a name="configuration"> </a>CONFIGURATION
* `/admin/structure/mason` to build a Mason grid.
* `/admin/structure/views`, and create a new page or block with Mason style.
* Use the provided sample to begin with, be sure to read its README.md.


***
***
# <a name="features"> </a>FEATURES
* Lazyloaded inline images, or CSS background images.
* Easy captioning.
* A few simple box layouts.
* Supports Colorbox/Photobox/PhotoSwipe via Blazy formatters.
* Views style plugin.

***
***
## <a name="troubleshooting"></a>TROUBLESHOOTING
* Not compatible with Responsive image OOTB, yet.

## TIPS:
* The key is: **floated elements as a normal CSS layout**. Failing to understand
  this, making the gapless grid is a PITA.
* Do not rely on random sizes, use proper calculation, or a simple math.
* If you see the samples broken, be sure to put them at wider regions such as
  `Hero` regions. Alternatively, edit the optionset largest `Columns` to better
  fit your content max-width. Both samples assumes large `Columns`:
  `1080, 1920, 12` which means `a min-width 1080px to max-width 1920px with
  12 columns`. They will not display correctly on smaller regions which
  may fit with smaller `Columns` option such as, says: `640, 768, 12`. To
  understand it better, inspect elements by F12, and see the generated
  dimensions on `.mason__box` selectors. Adjust the `Columns` option
  accordingly.


## PROMOTED OPTION
You can tell mason to promote specific elements if you want by assigning a class
and telling mason in the config. Notice in this grid nothing changes on refresh.


## CURRENT DEVELOPMENT STATUS
A full release should be reasonable after proper feedback from the community,
some code cleanup, and optimization where needed. Patches are very much welcome.

Alpha and Beta releases are for developers only. Be aware of possible breakage.

However if it is broken, unless an update is explicitly required, clearing cache
should fix most issues durig DEV phases. Prior to any update, always visit:
`/admin/config/development/performance`


***
***
# <a name="roadmap"> </a>ROADMAP
* [x] Support lazyload for inline images. Currently only CSS background images.
* Support Responsive image to have various sizes, only if doable.


***
***
# <a name="faq"> </a>FAQ
## HOW DOES IT WORK?
Mason works by flowing a grid of floated elements as a normal CSS layout, then
measuring the dimensions of the blocks and total grid area. It then detects
where gaps are and fills them.

It uses fillers to fill in gaps. Fillers are elements that you can define or it
will reuse elements within the grid. If fillers are ugly, use Promoted option
with proper calculation and most likely a couple of trials and errors. Be sure
the amount of visible mason boxes are matching the amount of Promoted items.

## REGISTERING SKINS
1. Copy `\Drupal\mason\Plugin\mason\MasonSkin` into your module
  `/src/Plugin/mason directory`.
2. Adjust everything accordingly: rename the file, change MasonSkin ID and
  label, change class name and its namespace, define skin name, and its CSS and
  JS assets.

The MasonSkin object has 1 supported method: `::setSkins()`.

The declared skins will be available for custom coded, or UI selections.
Be sure to clear cache since skins are permanently cached!

***
***
# <a name="maintainers"> </a>MAINTAINERS/CREDITS
* [Gaus Surahman](https://www.drupal.org/user/159062)
* [Contributors](https://www.drupal.org/node/2657012/committers)
* CHANGELOG.txt for helpful souls with their patches, suggestions and reports.


## READ MORE
See the project page on drupal.org: https://drupal.org/project/mason

See the Mason JS docs at:
* https://github.com/DrewDahlman/Mason
* https://masonjs.com/
