<?php

/**
 * @file
 * Hooks and preprocess functions for the Mason module.
 */

use Drupal\blazy\Blazy;
use Drupal\mason\MasonDefault;

/**
 * Prepares variables for mason.html.twig templates.
 */
function template_preprocess_mason(array &$variables) {
  $element = $variables['element'];
  foreach (MasonDefault::themeProperties() as $key => $default) {
    $variables[$key] = $element["#$key"] ?? $default;
  }

  // Prepare attributes, and add the configuration as JSON object.
  $settings = &$variables['settings'];
  mason()->verifySafely($settings);

  $id = mason()->getHtmlId('mason', $settings['id']);

  $attributes = &$variables['attributes'];
  $attributes['id'] = $id;

  $attributes['data-mason'] = $variables['json'];
  Blazy::containerAttributes($attributes, $settings);

  // Pass settings and attributes to twig.
  $classes = (array) ($attributes['class'] ?? []);
  if (strpos($id, 'mason-') !== FALSE) {
    $classes[] = str_replace('mason-', 'mason--', $id);
  }

  $attributes['class']  = array_merge(['mason'], $classes);
  $variables['blazies'] = $settings['blazies']->storage();
  $variables['masons']  = $settings['masons']->storage();
}

/**
 * Prepares variables for mason-box.html.twig templates.
 */
function template_preprocess_mason_box(array &$variables) {
  foreach (['delta', 'item', 'settings'] as $key) {
    $default = $key == 'delta' ? 0 : [];
    $variables[$key] = $variables['element']["#$key"] ?? $default;
  }

  // Boxes may have captions.
  $item = &$variables['item'];
  foreach (MasonDefault::themeCaptions() as $key) {
    $item['caption'][$key] = $item['caption'][$key] ?? [];
  }

  $attributes      = &$variables['attributes'];
  $settings        = $variables['settings'];
  $fillers         = (int) ($settings['fillers'] ?? 0);
  $item['box']     = $item['box'] ?? [];
  $item['caption'] = array_filter($item['caption']);

  $variables['fillers'] = $fillers
    && ($variables['delta'] > $fillers);

  $classes = (array) ($attributes['class'] ?? []);
  $attributes['class'] = array_merge([
    ($variables['fillers'] ? 'mason__fill' : 'mason__box'),
    'box',
  ], $classes);

  $variables['blazies'] = $settings['blazies']->storage();
  $variables['masons']  = $settings['masons']->storage();
}
