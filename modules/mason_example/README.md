
# INTRODUCTION
The Mason sample requires a "field_image" normally at Article content type and
automatically created when using Standard profile.

If no "field_image", simply create a field named "Image".

If using the provided sample, be sure to use Blazy formatter:
https://www.drupal.org/project/blazy

Otherwise empty images. Edit the sample here:

`/admin/structure/views/view/mason_x/edit`
