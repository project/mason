<?php

namespace Drupal\mason_example\Plugin\mason;

use Drupal\mason\MasonSkinPluginBase;

/**
 * Provides mason example skins.
 *
 * @MasonSkin(
 *   id = "mason_example_skin",
 *   label = @Translation("Mason example skin")
 * )
 */
class MasonExampleSkin extends MasonSkinPluginBase {

  /**
   * Sets the mason skins.
   *
   * @inheritdoc
   */
  protected function setSkins() {
    // If you copy this file, be sure to add base_path() before any asset path
    // (css or js) as otherwise failing to load the assets. Your module can
    // register paths pointing to a theme.
    $path = \blazy()->getPath('module', 'mason_example', TRUE);
    return [
      'zoe' => [
        'name' => 'X: Zoe',
        'provider' => 'mason_example',
        'css' => [
          'theme' => [
            $path . '/css/mason.theme--zoe.css' => [],
          ],
        ],
      ],
    ];
  }

}
