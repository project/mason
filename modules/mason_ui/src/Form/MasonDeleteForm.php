<?php

namespace Drupal\mason_ui\Form;

use Drupal\Core\Url;
use Drupal\blazy\Form\BlazyDeleteFormBase;

/**
 * Builds the form to delete a Mason optionset.
 */
class MasonDeleteForm extends BlazyDeleteFormBase {

  /**
   * Defines the nice anme.
   *
   * @var string
   */
  protected static $niceName = 'Mason';

  /**
   * Defines machine name.
   *
   * @var string
   */
  protected static $machineName = 'mason';

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.mason.collection');
  }

}
