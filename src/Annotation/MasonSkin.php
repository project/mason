<?php

namespace Drupal\mason\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a MasonSkin item annotation object.
 *
 * @Annotation
 */
class MasonSkin extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
