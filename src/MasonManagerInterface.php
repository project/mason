<?php

namespace Drupal\mason;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\blazy\BlazyManagerBaseInterface;
use Drupal\mason\Entity\Mason;

/**
 * Defines re-usable services and functions for mason plugins.
 */
interface MasonManagerInterface extends BlazyManagerBaseInterface, TrustedCallbackInterface {

  /**
   * Returns mason skin manager service.
   */
  public function skinManager();

  /**
   * Returns a cacheable renderable array of a single mason instance.
   *
   * @param array $build
   *   An associative array containing:
   *   - items: An array of mason contents: text, image or media.
   *   - options: An array of key:value pairs of custom JS options.
   *   - optionset: The cached optionset object to avoid multiple invocations.
   *   - settings: An array of key:value pairs of HTML/layout related settings.
   *
   * @return array
   *   The cacheable renderable array of a mason instance, or empty array.
   */
  public function build(array $build): array;

  /**
   * Load the optionset with a fallback.
   *
   * @param string $name
   *   The optionset name.
   *
   * @return \Drupal\mason\Entity\Mason
   *   The optionset object.
   */
  public function loadSafely($name): Mason;

}
