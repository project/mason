<?php

namespace Drupal\mason\Entity;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Mason configuration entity.
 *
 * @ConfigEntityType(
 *   id = "mason",
 *   label = @Translation("Mason optionset"),
 *   list_path = "admin/structure/mason",
 *   config_prefix = "optionset",
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label",
 *     "status" = "status",
 *     "weight" = "weight",
 *   },
 *   config_export = {
 *     "id",
 *     "name",
 *     "weight",
 *     "label",
 *     "options",
 *     "json",
 *   }
 * )
 */
class Mason extends ConfigEntityBase implements MasonInterface {

  /**
   * The legacy CTools ID for the configurable optionset.
   *
   * @var string
   */
  protected $name;

  /**
   * The human-readable name for the optionset.
   *
   * @var string
   */
  protected $label;

  /**
   * The weight to re-arrange the order of mason optionsets.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * The plugin instance json to reduce frontend logic.
   *
   * @var string
   */
  protected $json = '';

  /**
   * The plugin instance options.
   *
   * @var array
   */
  protected $options = [];

  /**
   * Overrides Drupal\Core\Entity\Entity::id().
   */
  public function id() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions($group = NULL, $property = NULL) {
    if ($group) {
      if (is_array($group)) {
        return NestedArray::getValue($this->options, $group);
      }
      elseif (isset($property) && isset($this->options[$group])) {
        return $this->options[$group][$property] ?? NULL;
      }
      return $this->options[$group] ?? $this->options;
    }
    return $this->options;
  }

  /**
   * {@inheritdoc}
   */
  public function getOption($option_name) {
    return $this->options[$option_name] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getJson(): string {
    return $this->json ?? '';
  }

  /**
   * Load the optionset with a fallback.
   *
   * @param string $name
   *   The optionset name.
   *
   * @return object
   *   The optionset object.
   */
  public static function loadSafely($name) {
    $optionset = self::load($name);

    // Ensures deleted optionset while being used doesn't screw up.
    return empty($optionset) ? self::load('default') : $optionset;
  }

  /**
   * If optionset does not exist, load one.
   *
   * @param array $build
   *   The array containing normally settings, optionset, items, etc.
   * @param string $name
   *   The optionset name.
   *
   * @return object
   *   The optionset object.
   */
  public static function verifyOptionset(array &$build, $name) {
    // The element is normally present at template_preprocess, not builders.
    $key = isset($build['element']) ? 'optionset' : '#optionset';
    if (empty($build[$key])) {
      $build[$key] = self::loadSafely($name);
    }
    // Also returns it for convenient.
    return $build[$key];
  }

  /**
   * Load the optionset with a fallback.
   *
   * @param string $id
   *   The optionset name.
   *
   * @return object
   *   The optionset object.
   *
   * @todo deprecated in blazy:8.x-2.17 and is removed from blazy:3.0.0. Use
   *   self::loadSafely() instead.
   * @see https://www.drupal.org/node/3103018
   */
  public static function loadWithFallback($id) {
    return self::loadSafely($id);
  }

}
