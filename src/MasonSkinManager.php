<?php

namespace Drupal\mason;

use Drupal\Component\Plugin\Mapper\MapperInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides mason skin manager service.
 */
class MasonSkinManager extends DefaultPluginManager implements MasonSkinManagerInterface, MapperInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Static cache for the skin definition.
   *
   * @var array
   */
  protected $skinDefinition;

  /**
   * The library info definition.
   *
   * @var array
   */
  protected $libraryInfoBuild;

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config) {
    parent::__construct('Plugin/mason', $namespaces, $module_handler, MasonSkinPluginInterface::class, 'Drupal\mason\Annotation\MasonSkin');

    $this->config = $config;
    $this->alterInfo('mason_skin_info');
    $this->setCacheBackend($cache_backend, 'mason_skin_plugins');
  }

  /**
   * Returns mason config shortcut.
   */
  public function config($key = '', $settings = 'mason.settings') {
    return $this->config->get($settings)->get($key);
  }

  /**
   * Returns cache backend service.
   */
  public function getCache() {
    return $this->cacheBackend;
  }

  /**
   * {@inheritdoc}
   */
  public function load($plugin_id) {
    return $this->createInstance($plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple() {
    $skins = [];
    foreach ($this->getDefinitions() as $definition) {
      array_push($skins, $this->createInstance($definition['id']));
    }
    return $skins;
  }

  /**
   * {@inheritdoc}
   */
  public function getSkins(): array {
    if (!isset($this->skinDefinition)) {
      $cid = 'mason_skins_data';
      $cache = $this->cacheBackend->get($cid);

      if ($cache && $data = $cache->data) {
        $this->skinDefinition = $data;
      }
      else {
        $skins = [];
        foreach ($this->loadMultiple() as $skin) {
          $skins = NestedArray::mergeDeep($skins, $skin->skins());
        }

        $count = count($skins);
        $tags = Cache::buildTags($cid, ['count:' . $count]);
        $this->cacheBackend->set($cid, $skins, Cache::PERMANENT, $tags);

        $this->skinDefinition = $skins;
      }
    }
    return $this->skinDefinition;
  }

  /**
   * {@inheritdoc}
   */
  public function libraryInfoBuild(): array {
    if (!isset($this->libraryInfoBuild)) {
      $libraries = [];
      if ($skins = $this->getSkins()) {
        foreach ($skins as $key => $skin) {
          $provider = $skin['provider'] ?? 'mason';
          $id = $provider . '.' . $key;
          foreach (['css', 'js', 'dependencies'] as $property) {
            if (isset($skin[$property]) && is_array($skin[$property])) {
              $libraries[$id][$property] = $skin[$property];
            }
          }
          $libraries[$id]['dependencies'][] = 'mason/load';
        }
      }

      $this->libraryInfoBuild = $libraries;
    }
    return $this->libraryInfoBuild;
  }

}
