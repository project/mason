<?php

namespace Drupal\mason\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'Mason Entity' formatter.
 *
 * @FieldFormatter(
 *   id = "mason_entity",
 *   label = @Translation("Mason Entity"),
 *   description = @Translation("Display the entity reference as a Mason."),
 *   field_types = {"entity_reference"},
 *   quickedit = {"editor" = "disabled"}
 * )
 */
class MasonEntityFormatter extends MasonEntityFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $storage = $field_definition->getFieldStorageDefinition();

    return $storage->isMultiple() && $storage->getSetting('target_type') !== 'media';
  }

}
