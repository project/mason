<?php

namespace Drupal\mason\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'Mason Image' formatter.
 *
 * @FieldFormatter(
 *   id = "mason_image",
 *   label = @Translation("Mason Image"),
 *   description = @Translation("Display the images as a Mason."),
 *   field_types = {"image"},
 *   quickedit = {"editor" = "disabled"}
 * )
 */
class MasonImageFormatter extends MasonFileFormatterBase {}
