<?php

namespace Drupal\mason\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\blazy\Plugin\Field\FieldFormatter\BlazyFormatterTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Trait common for mason formatters.
 */
trait MasonFormatterTrait {

  use BlazyFormatterTrait {
    injectServices as blazyInjectServices;
    getCommonFieldDefinition as blazyCommonFieldDefinition;
  }

  /**
   * Overrides the blazy manager.
   */
  public function blazyManager() {
    return $this->formatter;
  }

  /**
   * Injects DI services.
   */
  protected static function injectServices($instance, ContainerInterface $container, $type = '') {
    $instance = static::blazyInjectServices($instance, $container, $type);
    $instance->formatter = $instance->blazyManager = $container->get('mason.formatter');
    $instance->manager = $container->get('mason.manager');

    return $instance;
  }

  /**
   * Returns the mason admin service shortcut.
   */
  public function admin() {
    return \Drupal::service('mason.admin');
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getFieldStorageDefinition()->isMultiple();
  }

  /**
   * {@inheritdoc}
   */
  protected function pluginSettings(&$blazies, array &$settings): void {
    $blazies->set('is.blazy', TRUE)
      // @todo remove post blazy:2.18.
      ->set('item.id', 'box')
      ->set('namespace', 'mason');
  }

  /**
   * Defines the common scope for both front and admin.
   */
  public function getCommonFieldDefinition() {
    return [
      'namespace' => 'mason',
      'style'     => FALSE,
      'grid_form' => FALSE,
    ] + $this->blazyCommonFieldDefinition();
  }

}
