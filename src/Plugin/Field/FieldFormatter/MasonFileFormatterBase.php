<?php

namespace Drupal\mason\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\blazy\Plugin\Field\FieldFormatter\BlazyFormatterBlazy;
use Drupal\mason\MasonDefault;

/**
 * Base class for mason image and file ER formatters.
 */
abstract class MasonFileFormatterBase extends BlazyFormatterBlazy {

  use MasonFormatterTrait;

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'mason';

  /**
   * {@inheritdoc}
   */
  protected static $itemId = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $itemPrefix = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $captionId = 'caption';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return MasonDefault::imageSettings() + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    if (isset($element['image_style'])) {
      $element['image_style']['#description'] = $this->t('This will be treated as the fallback image style if the optionset image styles not provided.');
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginScopes(): array {
    return [
      'namespace'  => 'mason',
      'background' => TRUE,
      'no_ratio'   => TRUE,
    ] + parent::getPluginScopes();
  }

}
