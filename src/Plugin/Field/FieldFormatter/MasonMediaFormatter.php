<?php

namespace Drupal\mason\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\blazy\Plugin\Field\FieldFormatter\BlazyMediaFormatterBase;
use Drupal\mason\MasonDefault;

/**
 * Plugin implementation of the 'Mason Media' formatter.
 *
 * @FieldFormatter(
 *   id = "mason_media",
 *   label = @Translation("Mason Media"),
 *   description = @Translation("Display the core Media as a Mason."),
 *   field_types = {"entity_reference"},
 *   quickedit = {"editor" = "disabled"}
 * )
 */
class MasonMediaFormatter extends BlazyMediaFormatterBase {

  use MasonFormatterTrait;

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'mason';

  /**
   * {@inheritdoc}
   */
  protected static $itemId = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $itemPrefix = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $captionId = 'caption';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return MasonDefault::extendedSettings() + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginScopes(): array {
    return [
      'breakpoints' => FALSE,
      'vanilla'     => TRUE,
      'no_ratio'    => TRUE,
    ] + parent::getPluginScopes();
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $storage = $field_definition->getFieldStorageDefinition();

    return $storage->isMultiple() && $storage->getSetting('target_type') === 'media';
  }

}
