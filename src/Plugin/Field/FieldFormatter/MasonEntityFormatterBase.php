<?php

namespace Drupal\mason\Plugin\Field\FieldFormatter;

use Drupal\blazy\Field\BlazyEntityVanillaBase;
use Drupal\mason\MasonDefault;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for mason entity reference formatters without field details.
 */
abstract class MasonEntityFormatterBase extends BlazyEntityVanillaBase {

  use MasonFormatterTrait {
    buildSettings as traitBuildSettings;
  }

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'mason';

  /**
   * {@inheritdoc}
   */
  protected static $itemId = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $itemPrefix = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $captionId = 'caption';

  /**
   * {@inheritdoc}
   */
  protected static $fieldType = 'entity';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    return static::injectServices($instance, $container, static::$fieldType);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = MasonDefault::baseSettings();
    $settings['view_mode'] = '';

    return $settings + parent::defaultSettings();
  }

  /**
   * Builds the settings.
   */
  public function buildSettings() {
    return ['vanilla' => TRUE] + $this->traitBuildSettings();
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginScopes(): array {
    return [
      'breakpoints' => [],
      'vanilla'     => TRUE,
      'no_ratio'    => TRUE,
    ] + parent::getPluginScopes();
  }

}
