<?php

namespace Drupal\mason\Plugin\mason;

use Drupal\mason\MasonSkinPluginBase;

/**
 * Provides mason skins.
 *
 * @MasonSkin(
 *   id = "mason_skin",
 *   label = @Translation("Mason skin")
 * )
 */
class MasonSkin extends MasonSkinPluginBase {

  /**
   * Sets the mason skins.
   *
   * @inheritdoc
   */
  protected function setSkins() {
    // If you copy this file, be sure to add base_path() before any asset path
    // (css or js) as otherwise failing to load the assets. Your module can
    // register paths pointing to a theme.
    return [
      'default' => [
        'name' => 'Default',
        'provider' => 'mason',
        'css' => [
          'theme' => [
            'css/theme/mason.theme--default.css' => [],
          ],
        ],
      ],
      'selena' => [
        'name' => 'Selena',
        'provider' => 'mason',
        'css' => [
          'theme' => [
            'css/theme/mason.theme--selena.css' => [],
          ],
        ],
      ],
    ];
  }

}
