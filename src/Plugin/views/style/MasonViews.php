<?php

namespace Drupal\mason\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\blazy\Views\BlazyStylePluginBase;
use Drupal\mason\MasonDefault;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Mason style plugin.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "mason",
 *   title = @Translation("Mason"),
 *   help = @Translation("Display the results in a Mason."),
 *   theme = "mason",
 *   register_theme = FALSE,
 *   display_types = {"normal"}
 * )
 */
class MasonViews extends BlazyStylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'mason';

  /**
   * {@inheritdoc}
   */
  protected static $itemId = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $itemPrefix = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $captionId = 'caption';

  /**
   * The mason service manager.
   *
   * @var \Drupal\mason\MasonManagerInterface
   */
  protected $manager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->manager = $container->get('mason.manager');
    $instance->blazyManager = $instance->blazyManager ?? $container->get('blazy.manager');

    return $instance;
  }

  /**
   * Returns the mason manager.
   */
  public function manager() {
    return $this->manager;
  }

  /**
   * Returns the mason admin.
   */
  public function admin() {
    return \Drupal::service('mason.admin');
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = ['fillers' => ['default' => ''], 'stamp' => ['default' => []]];
    foreach (MasonDefault::extendedSettings() as $key => $value) {
      $options[$key] = ['default' => $value];
    }
    return $options + parent::defineOptions();
  }

  /**
   * Overrides parent::buildOptionsForm().
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $fields = MasonDefault::viewFieldOptions();
    $definition = $this->getDefinedFieldOptions($fields);
    $definition['_views'] = TRUE;

    $this->admin()->buildSettingsForm($form, $definition);

    $title = '<p class="form__header form__title">';
    $title .= $this->t('Check Vanilla if using content/custom markups, not fields. <small>See it under <strong>Format > Show</strong> section. Otherwise Gridstack markups apply which require some fields added below.</small>');
    $title .= '</p>';

    $form['opening']['#markup'] .= $title;
    $form['image']['#description'] .= ' ' . $this->t('Be sure to UNCHECK "Use field template" (by default already UNCHECKED) to have it work for Blazy lazyloading. Use Blazy formatters for relevant features such as Colorbox/Photobox/Photoswipe, etc., or multimedia supports.');
  }

  /**
   * {@inheritdoc}
   */
  protected function buildSettings() {
    $settings = parent::buildSettings();
    $this->manager->verifySafely($settings);
    $blazies = $settings['blazies'];

    // @todo remove post blazy:2.17.
    $blazies->set('item.id', static::$itemId)
      ->set('item.prefix', static::$itemPrefix)
      ->set('item.caption', static::$captionId)
      ->set('namespace', static::$namespace)
      ->set('is.noratio', TRUE);

    // Prepare needed settings to work with.
    $settings['caption'] = array_filter($settings['caption']);
    $settings['ratio']   = '';

    return $settings;
  }

  /**
   * Overrides StylePluginBase::render().
   *
   * @todo use self::buildSettings() post blazy:1.x update.
   */
  public function render() {
    $settings = $this->buildSettings();
    $elements = [];

    foreach ($this->renderGrouping($this->view->result, $settings['grouping']) as $rows) {
      $items = $this->buildElements($settings, $rows);

      // Supports lightbox gallery if using Blazy formatter.
      $build = ['items' => $items];
      $this->checkBlazy($settings, $build, $rows);

      $build['#settings'] = $settings;

      $elements = $this->manager->build($build);
    }

    return $elements;
  }

  /**
   * Returns mason contents.
   */
  public function buildElements(array $settings, $rows) {
    $build = [];

    foreach ($rows as $index => $row) {
      $this->view->row_index = $index;

      $sets = $settings;
      $box = [];
      $box['delta'] = $index;
      $box[static::$itemId] = [];
      $box['#settings'] = $sets;

      if (!empty($sets['class'])) {
        $classes = $this->getFieldString($row, $sets['class'], $index);
        $box['#settings']['class'] = empty($classes[$index]) ? [] : $classes[$index];
      }

      // Use Vanilla mason if so configured, ignoring Mason markups.
      if (!empty($sets['vanilla'])) {
        $box[static::$itemId] = $this->view->rowPlugin->render($row);
      }
      else {
        // Build individual row/element contents.
        $this->buildElement($box, $row, $index);
      }

      // Build mason items.
      $build[] = $box;
      unset($box);
    }

    unset($this->view->row_index);
    return $build;
  }

}
