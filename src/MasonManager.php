<?php

namespace Drupal\mason;

use Drupal\blazy\BlazyManagerBase;
use Drupal\mason\Entity\Mason;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides mason manager service.
 *
 * @todo lazyload[x], granular image styles, decent skins, stamps.
 */
class MasonManager extends BlazyManagerBase implements MasonManagerInterface {

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'mason';

  /**
   * {@inheritdoc}
   */
  protected static $itemId = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $itemPrefix = 'box';

  /**
   * The mason skin manager service.
   *
   * @var \Drupal\mason\MasonSkinManagerInterface
   */
  protected $skinManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->setSkinManager($container->get('mason.skin_manager'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRenderMason'];
  }

  /**
   * {@inheritdoc}
   */
  public function skinManager() {
    return $this->skinManager;
  }

  /**
   * Sets mason skin manager service.
   */
  public function setSkinManager(MasonSkinManagerInterface $skin_manager) {
    $this->skinManager = $skin_manager;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $build): array {
    foreach (MasonDefault::themeProperties() as $key => $default) {
      $k = in_array($key, ['items', 'postscript']) ? $key : "#$key";
      $build[$k] = $this->toHashtag($build, $key) ?: $default;
    }

    $mason = [
      '#theme'      => 'mason',
      '#items'      => [],
      '#build'      => $build,
      '#pre_render' => [[$this, 'preRenderMason']],
      'items'       => [],
    ];

    $this->moduleHandler->alter('mason_build', $mason);
    return empty($build['items']) ? [] : $mason;
  }

  /**
   * {@inheritdoc}
   */
  public function loadSafely($name): Mason {
    return Mason::loadSafely($name);
  }

  /**
   * {@inheritdoc}
   */
  public function preRenderMason($element) {
    $build = $element['#build'];
    unset($element['#build']);

    // Build mason elements.
    $settings = &$build['#settings'];
    $this->verifySafely($settings);

    $optionset = Mason::verifyOptionset($build, $settings['optionset']);

    $settings['_count']    = count($optionset->getOption('sizes'));
    $element['#optionset'] = $optionset;
    $element['#settings']  = $settings;
    $element['#items']     = $this->buildItems($build);
    $element['#json']      = $optionset->getJson();

    $this->setAttachments($element, $settings);

    unset($build);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function toBlazy(array &$data, array &$captions, $delta): void {
    $settings = $this->toHashtag($data);
    $this->verifySafely($settings);

    $blazies = $settings['blazies'];
    $prefix  = 'box';

    $blazies->set('item.id', $prefix)
      ->set('item.prefix', $prefix);

    $data['#wrapper_attributes']['class'][] = $prefix . '__content';
    if ($attrs = $blazies->get('item.wrapper_attributes', [])) {
      $data['#wrapper_attributes'] = $this->merge($data['#wrapper_attributes'], $attrs);
    }
    // @todo enable blazy:2.17: parent::toBlazy($data, $captions, $delta);
  }

  /**
   * {@inheritdoc}
   *
   * @todo remove at 3.x.
   */
  public function verify(array &$settings): void {
    parent::verify($settings);

    MasonDefault::verify($settings);
  }

  /**
   * {@inheritdoc}
   */
  public function verifySafely(array &$settings, $key = 'blazies', array $defaults = []) {
    MasonDefault::verify($settings);

    return parent::verifySafely($settings, $key, $defaults);
  }

  /**
   * {@inheritdoc}
   */
  protected function attachments(array &$load, array $attach, $blazies): void {
    $this->verifySafely($attach);

    $attach['blazy'] = TRUE;
    parent::attachments($load, $attach, $blazies);

    $load['library'][] = 'mason/load';

    if (isset($attach['skin']) && $skin = $attach['skin']) {
      $skins = $this->skinManager->getSkins();
      $provider = $skins[$skin]['provider'] ?? 'mason';
      $load['library'][] = 'mason/' . $provider . '.' . $skin;
    }

    $load['drupalSettings']['mason'] = Mason::load('default')->getOptions();

    $this->moduleHandler->alter('mason_attach', $load, $attach, $blazies);
  }

  /**
   * Returns grid items.
   */
  protected function buildItems(array $build) {
    $settings = $build['#settings'];
    $items    = [];

    foreach ($build['items'] as $delta => $item) {
      if ($delta > $settings['_count'] - 1) {
        continue;
      }

      $attrs = $this->toHashtag($item, 'attributes');
      $sets  = $this->toHashtag($item);
      $sets += $settings;

      $content = [
        'box'     => $this->toHashtag($item, 'box'),
        'caption' => $this->toHashtag($item, 'caption'),
      ];

      $box = [
        '#theme'      => 'mason_box',
        '#item'       => $content,
        '#delta'      => $delta,
        '#attributes' => $attrs,
        '#settings'   => $sets,
        '#cache'      => $item['#cache'] ?? [],
      ];

      $items[] = $box;
      unset($box);
    }

    return $items;
  }

  /**
   * {@inheritdoc}
   *
   * @todo remove post blazy:2.17.
   */
  public function verifyItem(array &$element, $delta): void {
    // Do nothing.
  }

}
