<?php

namespace Drupal\mason\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\blazy\Form\BlazyAdminInterface;
use Drupal\mason\MasonManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides resusable admin functions or form elements.
 */
class MasonAdmin implements MasonAdminInterface {

  use StringTranslationTrait;

  /**
   * The blazy admin service.
   *
   * @var \Drupal\blazy\Form\BlazyAdminInterface
   */
  protected $blazyAdmin;

  /**
   * The mason manager service.
   *
   * @var \Drupal\mason\MasonManagerInterface
   */
  protected $manager;

  /**
   * Static cache for the skin options.
   *
   * @var array
   */
  protected $skinOptions;

  /**
   * Constructs a MasonAdmin object.
   */
  public function __construct(
    BlazyAdminInterface $blazy_admin,
    MasonManagerInterface $manager,
  ) {
    $this->blazyAdmin = $blazy_admin;
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('blazy.admin.formatter'),
      $container->get('mason.manager')
    );
  }

  /**
   * Returns the blazy admin.
   */
  public function blazyAdmin() {
    return $this->blazyAdmin;
  }

  /**
   * Returns the mason manager.
   */
  public function manager() {
    return $this->manager;
  }

  /**
   * Returns all settings form elements.
   */
  public function buildSettingsForm(array &$form, $definition = []) {
    $definition['namespace']  = 'mason';
    $definition['skins']      = $this->getSkinOptions();
    $definition['optionsets'] = $this->blazyAdmin->getOptionsetOptions('mason');
    $definition['style']      = FALSE;
    $definition['grid_form']  = FALSE;
    $definition['layouts']    = isset($definition['layouts']) ? array_merge($this->getLayoutOptions(), $definition['layouts']) : $this->getLayoutOptions();

    foreach (['background', 'caches', 'fieldable_form', 'vanilla'] as $key) {
      $definition[$key] = $definition[$key] ?? TRUE;
    }

    $this->openingForm($form, $definition);
    $this->mainForm($form, $definition);
    $this->closingForm($form, $definition);
  }

  /**
   * Returns the opening form elements.
   */
  public function openingForm(array &$form, &$definition = []) {
    $path   = \blazy()->getPath('module', 'mason');
    $readme = Url::fromUri('base:' . $path . '/README.md')->toString();

    if (!isset($form['optionset'])) {
      $this->blazyAdmin->openingForm($form, $definition);
    }

    $form['skin']['#description'] = $this->t('Skins allow various layouts with just CSS. Some options below depend on a skin. Leave empty to DIY. Or copy and adjust \Drupal\mason\Plugin\mason\MasonSkin to register ones.', [':url' => $readme]);
    $form['background']['#description'] = $this->t('If trouble with image sizes not filling the given box, check this to turn the image into CSS background instead. To assign different image style per grid/box, edit the working optionset.');
  }

  /**
   * Returns the main form elements.
   */
  public function mainForm(array &$form, $definition = []) {
    if (!empty($definition['image_style_form'])) {
      $this->blazyAdmin->imageStyleForm($form, $definition);
    }

    if (!empty($definition['media_switch_form'])) {
      $this->blazyAdmin->mediaSwitchForm($form, $definition);
    }

    if (!empty($definition['fieldable_form'])) {
      $this->blazyAdmin->fieldableForm($form, $definition);

      if (!empty($definition['links'])) {
        $form['category'] = [
          '#title'         => $this->t('Category'),
          '#type'          => 'select',
          '#options'       => $definition['links'],
          '#description'   => $this->t('The category to display at the top of box.'),
        ];
      }
    }

    if (isset($form['overlay'])) {
      $form['overlay']['#title'] = $this->t('Rich box');
      $form['overlay']['#description'] = $this->t('Replace <b>Main stage/ image</b> if a node has one. It can be any entity reference, like Block, etc. Use block_field.module for ease of block additions. How? Create a sticky (or far future created) node or two containing a Slick carousel, video, weather, time, donations, currency, ads, or anything as a block field, and put it here. Be sure different from the <b>Main stage</b>. While regular boxes are updated with new contents, these rich boxes may stay the same, and sticky (Use Views with sort by sticky or desc by creation).');
      $form['overlay']['#weight'] = -65;
    }

    $form['fillers'] = [
      '#type'        => 'select',
      '#title'       => $this->t('Filler start at'),
      '#options'     => array_combine(range(1, 42), range(1, 42)),
      '#description' => $this->t('Index to mark contents as fillers. Contents starting from this value will be treated as fillers. Be sure the total Views rows are bigger than this. Mason uses fillers to fill in gaps. Fillers are elements that you can define or it will reuse elements within the grid. Leave it empty to disable fillers, and use Promoted option instead to control sizes.'),
    ];
  }

  /**
   * Returns the closing form elements.
   */
  public function closingForm(array &$form, $definition = []) {
    if (!isset($form['cache'])) {
      $this->blazyAdmin->closingForm($form, $definition);
    }

    $form['#attached']['library'][] = 'mason/admin';
  }

  /**
   * Returns available mason skins for select options.
   */
  public function getSkinOptions() {
    if (!isset($this->skinOptions)) {
      $skins = [];
      foreach ($this->manager->skinManager()->getSkins() as $skin => $properties) {
        $skins[$skin] = isset($properties['name']) ? Html::escape($properties['name']) : $skin;
      }
      $this->skinOptions = $skins;
    }
    return $this->skinOptions;
  }

  /**
   * Returns default layout options for the core Image, or Views.
   */
  public function getLayoutOptions() {
    return [
      'bottom' => $this->t('Caption bottom'),
      'center' => $this->t('Caption center'),
      'top'    => $this->t('Caption top'),
    ];
  }

  /**
   * Return the field formatter settings summary.
   */
  public function getSettingsSummary(array $definition = []): array {
    return $this->blazyAdmin->getSettingsSummary($definition);
  }

  /**
   * Returns available fields for select options.
   */
  public function getFieldOptions($target_bundles = [], $allowed_field_types = [], $entity_type_id = 'media', $target_type = '') {
    return $this->blazyAdmin->getFieldOptions($target_bundles, $allowed_field_types, $entity_type_id, $target_type);
  }

  /**
   * Returns re-usable logic, styling and assets across fields and Views.
   */
  public function finalizeForm(array &$form, $definition = []) {
    $this->blazyAdmin->finalizeForm($form, $definition);
  }

}
