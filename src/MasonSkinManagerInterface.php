<?php

namespace Drupal\mason;

/**
 * Provides an interface defining Mason skins, and asset managements.
 */
interface MasonSkinManagerInterface {

  /**
   * Returns cache backend service.
   */
  public function getCache();

  /**
   * Returns an instance of a plugin by given plugin id.
   *
   * @param string $id
   *   The plugin id.
   *
   * @return object
   *   Return instance of MasonSkin.
   */
  public function load($id);

  /**
   * Returns mason skins registered via MasonSkin plugin or defaults.
   */
  public function getSkins(): array;

  /**
   * Implements hook_library_info_build().
   */
  public function libraryInfoBuild(): array;

}
