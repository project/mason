<?php

namespace Drupal\mason;

use Drupal\blazy\BlazyFormatter;
use Drupal\mason\Entity\Mason;

/**
 * Provides mason formatter service.
 */
class MasonFormatter extends BlazyFormatter implements MasonFormatterInterface {

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'mason';

  /**
   * {@inheritdoc}
   */
  protected static $itemId = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $itemPrefix = 'box';

  /**
   * {@inheritdoc}
   */
  public function buildSettings(array &$build, $items) {
    // Prepare integration with Blazy.
    $settings = &$build['#settings'];

    $this->verifySafely($settings);

    // Pass basic info to parent::buildSettings().
    parent::buildSettings($build, $items);
  }

  /**
   * {@inheritdoc}
   */
  public function preBuildElements(array &$build, $items, array $entities = []) {
    parent::preBuildElements($build, $items, $entities);

    $settings = &$build['#settings'];

    // Mason specific stuffs.
    $build['#optionset'] = Mason::loadSafely($settings['optionset']);

    $this->moduleHandler()->alter('mason_settings', $build, $items);
  }

  /**
   * {@inheritdoc}
   *
   * @todo remove at 3.x.
   */
  public function verify(array &$settings): void {
    parent::verify($settings);

    MasonDefault::verify($settings);
  }

  /**
   * {@inheritdoc}
   */
  public function verifySafely(array &$settings, $key = 'blazies', array $defaults = []) {
    MasonDefault::verify($settings);

    return parent::verifySafely($settings, $key, $defaults);
  }

}
