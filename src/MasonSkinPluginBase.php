<?php

namespace Drupal\mason;

use Drupal\Core\Plugin\PluginBase;

/**
 * Provides base class for all mason skins.
 */
abstract class MasonSkinPluginBase extends PluginBase implements MasonSkinPluginInterface {

  /**
   * The mason main/thumbnail skin definitions.
   *
   * @var array
   */
  protected $skins;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->skins = $this->setSkins();
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function skins() {
    return $this->skins;
  }

  /**
   * Sets the required plugin main/thumbnail skins.
   */
  abstract protected function setSkins();

}
