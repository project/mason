<?php

namespace Drupal\mason;

use Drupal\blazy\BlazyDefault;

/**
 * Defines shared plugin default settings for field formatter and Views style.
 */
class MasonDefault extends BlazyDefault {

  /**
   * {@inheritdoc}
   *
   * @todo enable post blazy:2.17, and remove dup from self::htmlSettings().
   * protected static $id = 'masons';
   */

  /**
   * {@inheritdoc}
   */
  public static function baseSettings() {
    return [
      'optionset' => 'default',
      'skin'      => '',
      'vanilla'   => FALSE,
      'fillers'   => '',
    ] + parent::baseSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function imageSettings() {
    return [
      'background'  => TRUE,
      'category'    => '',
      'stamp'       => '',
      'stamp_index' => '',
    ] + self::baseSettings()
      + parent::imageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function extendedSettings() {
    return self::imageSettings() + parent::extendedSettings();
  }

  /**
   * Returns view fields options for Views UI.
   */
  public static function viewFieldOptions() {
    return [
      'captions',
      'layouts',
      'images',
      'links',
      'titles',
      'classes',
    ];
  }

  /**
   * Returns fixed settings not configurable via UI.
   */
  public static function fixedSettings() {
    return [
      'namespace' => 'mason',
      'ratio'     => '',
      'blazy'     => TRUE,
      'lazy'      => 'blazy',
    ];
  }

  /**
   * Returns HTML or layout related settings, none of JS to shutup notices.
   */
  public static function htmlSettings() {
    return [
      // @todo remove post 2.17:
      'masons'     => \blazy()->settings(),
      'background' => TRUE,

    ] + self::extendedSettings()
      + self::fixedSettings()
      + parent::htmlSettings();
  }

  /**
   * Returns theme properties.
   */
  public static function themeProperties() {
    return [
      'attributes' => [],
      'items' => [],
      'optionset' => NULL,
      'postscript' => [],
      'json' => '',
      'settings' => [],
    ];
  }

  /**
   * Returns theme captions.
   */
  public static function themeCaptions() {
    return ['alt', 'category', 'data', 'link', 'overlay', 'title'];
  }

  /**
   * Verify the settings.
   */
  public static function verify(array &$settings): void {
    if (!isset($settings['masons'])) {
      $settings += self::htmlSettings();
      $settings += self::entitySettings();
      $settings = array_merge($settings, self::fixedSettings());
    }
  }

}
