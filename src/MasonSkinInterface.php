<?php

namespace Drupal\mason;

/**
 * Provides an interface defining Mason skins.
 *
 * The hook_hook_info() is deprecated, and no resolution by 1/16/16:
 *   #2233261: Deprecate hook_hook_info()
 *     Postponed till D9
 *
 * @todo deprecated and remove anytime.
 */
interface MasonSkinInterface {}
