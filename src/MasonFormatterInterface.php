<?php

namespace Drupal\mason;

use Drupal\blazy\BlazyFormatterInterface;

/**
 * Defines re-usable services and functions for mason field plugins.
 */
interface MasonFormatterInterface extends BlazyFormatterInterface {}
