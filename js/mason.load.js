/**
 * @file
 * Provides Mason loader.
 */

(function ($, Drupal, drupalSettings, _d, _doc) {

  'use strict';

  var _id = 'mason';
  var _idOnce = _id;
  var _mounted = _id + '--on';
  var _element = '.' + _id + ':not(.' + _mounted + ')';

  /**
   * Mason utility functions.
   *
   * @param {HTMLElement} elm
   *   The HTML element.
   */
  function doMason(elm) {
    var $elm = $(elm);
    var options = $.extend({}, drupalSettings.mason, $elm.data('mason'));

    $elm.mason(options);
    _d.addClass(elm, _mounted);
  }

  /**
   * Attaches jumper behavior to HTML element identified by .mason.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.mason = {
    attach: function (context) {

      _d.once(doMason, _idOnce, _element, context);
    },
    detach: function (context, setting, trigger) {
      if (trigger === 'unload') {
        _d.once.removeSafely(_idOnce, _element, context);
      }
    }
  };

})(jQuery, Drupal, drupalSettings, dBlazy, this.document);
